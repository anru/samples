; ------------------------------------------------------------------------------
; This file is copied from https://github.com/leomil72/LM80C
; ------------------------------------------------------------------------------
; This code is intended to be used with LM80C Z80-based computer
; designed by Leonardo Miliani. More info at
; www DOT leonardomiliani DOT com
; ------------------------------------------------------------------------------
; Written by Leonardo Miliani
; Based on code samples from "How To Program the Z80 Periphery Tutorial" by Mario Blunk
; Edited with Atom Editor
; Compiled with ZASM assembler 4.2.4 on MacOS
; ------------------------------------------------------------------------------
; Released under the terms of GPL v.3 or any successive release
; ------------------------------------------------------------------------------
; This code is slightly modified to work with basic Z80 and PIO setup along with
; Ben Eater's clock module (https://eater.net/8bit/clock)
; ------------------------------------------------------------------------------

DATAREGA    equ 00000000b
DATAREGB    equ 00000001b
CTRLREGA    equ 00000010b
CTRLREGB    equ 00000011b

.org     0x0000

reset:  ; this corresponds to the RESET vector (0000h)

        ; delays are only one iteration as Ben Eater's clock module is used
        ld d, 0x01
loop3:
        ld b, 0x01
loop4:
        djnz loop4
        dec d
        jp nz, loop3
program:
        ; let's program the PIO
        ld a, 0b11001111      ; mode 3 (bit control) CF
        out (CTRLREGB), a
        ld a, 0b00000000      ; set pins of port B to OUTPUT
        out (CTRLREGB), a

        ld e, 0x55           ; this is the pattern start (all LEDs set to OFF)

noexit:
        ; send the pattern to the PIO
        ld a, e
        out (DATAREGB), a

        ld d, 0x01
loop1:
        ld b, 0x01
loop2:
        djnz loop2
        dec d
        jp nz, loop1

        ; rotate stored byte left
        rl e
        ; sent the new pattern to the PIO
        ld a, e
        out (DATAREGB), a
        jp noexit

    .org 0x7ffc
    .word 0xffff
    .word 0xffff