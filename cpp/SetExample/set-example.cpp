#include <set>
#include <iostream>
#include <random>
#include <climits>
#include <chrono>
#include <regex>

/*
 * With the help of cppreference.com
 */

uint MAX_COUNT = 100000000;

uint count = 0;
std::string file_name("");

std::default_random_engine generator;
std::uniform_int_distribution<int> distribution(1, INT_MAX);
// auto pattern = std::regex("([ad]) ([0-9]{1,10})");
std::regex pattern("([ad]) ([0-9]{1,10})");

int random_number()
{
    return distribution(generator);
}

void populate(std::set<int> &set, uint count)
{
    std::cout << ">    Populating hash set" << std::endl;
    std::chrono::high_resolution_clock::time_point start =
        std::chrono::high_resolution_clock::now();

    auto step = 1000;
    auto i = 0;
    while (set.size() < count) {
        int number = random_number();
        set.insert(number);

        if (i > 0 && (i % step) == 0) {
            int size = set.size();
            std::cout << "      Hash set populated with " << size
                << " items (at " << i << " iterations)" << std::endl;
            if (size > step * 100) {
                step *= 10;
            }
        }
        i++;
    }

    std::chrono::high_resolution_clock::time_point end =
        std::chrono::high_resolution_clock::now();
    std::chrono::duration<double, std::milli> time_span = end - start;
    std::cout << "<    Populating hash set finished in " << time_span.count()
        << "ms (" << set.size() << " items)" << std::endl;
}

void dump(std::set<int> set)
{
    std::cout << ">Dump\n";
    int i = 0;
    for (std::set<int>::iterator it = set.begin(); it != set.end(); ++it, i++) {
        std::cout << "    " << i << " " << *it << std::endl;
    }
}

void parse_arguments(std::vector<std::string> args)
{
    std::vector<std::string>::iterator it = args.begin();
    while (it != args.end()) {
        if (*it == "-r") {
            ++it;
            try {
                count = std::stoi(*it);
            } catch (...) {
                count = 0;
            }
        }
        if (*it == "-o") {
            std::cout << "Serializing not supported" << std::endl;
        }
        ++it;
    }
}

void usage()
{
    std::cout
        << "    a <number> add a number" << std::endl
        << "    d <number> remove a number" << std::endl
        << "    p          show set contents" << std::endl
        << "    ?          show this help" << std::endl
        << "    c          exit" << std::endl;
}

void handle_command(std::set<int> &set, std::string command, int value)
{
    auto it = set.find(value);
    if (command == "a") {
        if (it == set.end()) {
            set.insert(value);
            std::cout << "Value " << value << " added to set" << std::endl;
        } else {
            std::cout << "Value " << value << " already in set" << std::endl;
        }
    } else if (command == "d") {
        if (it == set.end()) {
            std::cout << "Value " << value << " not found in set" << std::endl;
        } else {
            set.erase(value);
            std::cout << "Value " << value << " removed from set" << std::endl;
        }
    }
}

int main(int argc, char *argv[])
{
    std::set<int> set;

    std::vector<std::string> args(argv + 1, argv + argc);
    parse_arguments(args);

    populate(set, count);

    std::string input;
    while (true) {
        std::cout << "Enter a number (c to exit): ";
        std::getline(std::cin, input);

        if (input == "c" || input == "q") {
            break;
        }

        if (input == "p") {
            dump(set);
            continue;
        }

        if (input == "?") {
            usage();
            continue;
        }

        std::smatch matches;
        if (std::regex_search(input, matches, pattern)) {
            if (matches.size() == 3) {
                int value;
                try {
                    value = std::stoi(matches[2].str());
                } catch (...) {
                    // continue with the input loop
                    continue;
                }

                handle_command(set, matches[1], value);
            }
        }
    }
    return 0;
}
