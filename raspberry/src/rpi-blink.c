/*
 *
 * Based on
 *  1. https://github.com/vovkos/rpi-gpio-test
 *  2. https://github.com/sysprogs/tutorials
 *
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/gpio.h>
#include <linux/sched.h>

#define LKM_NAME "rpi-blink"

static volatile unsigned int *g_gpio_regs;

#define GPIO_BASE_ADDR 0x3f200000

#define GPIO_SET_FUNC_IN(g) *(g_gpio_regs + ((g) / 10)) &= ~(7 << (((g) % 10) * 3))
#define GPIO_SET_FUNC_OUT(g) *(g_gpio_regs + ((g) / 10)) |= (1 << (((g) % 10) * 3))

#define GPIO_GET(g) ((*(g_gpio_regs + 13) & (1 << (g))) != 0)
#define GPIO_SET(g) (*(g_gpio_regs + 7) = 1 << (g))
#define GPIO_CLR(g) (*(g_gpio_regs + 10) = 1 << (g))

/*
 * Output pin set to 18, led is connected to it on one side and to
 * the ground through a resistor on the other side
 */
#define GPIO_LED_OUT 18

static struct timer_list blink_timer;
// LED will be on or off for 500ms
static int blink_period = 500;

static void set_GPIO(int pin, bool value)
{
    if (value) {
        GPIO_SET(pin);
    } else {
        GPIO_CLR(pin);
    }
}

static void timer_handler(struct timer_list *unused)
{
    // ensure we know the value of the GPIO pin between timer calls
    static bool on = false;

    // toggle GPIO pin (high or low)
    on = !on;
    set_GPIO(GPIO_LED_OUT, on);

    mod_timer(&blink_timer, jiffies + msecs_to_jiffies(blink_period));
}

static int __init rpi_blink_init(void)
{
    int result;

    printk(KERN_INFO LKM_NAME ": initializing\n");

    g_gpio_regs = (volatile unsigned int *)ioremap(GPIO_BASE_ADDR, 16 * 1024);
    if (!g_gpio_regs) {
        printk(KERN_INFO LKM_NAME ": could not map GPIO registers\n");
        return -EBUSY;
    }

    GPIO_SET_FUNC_OUT(GPIO_LED_OUT);

    timer_setup(&blink_timer, timer_handler, 0);
    result = mod_timer(&blink_timer, jiffies + msecs_to_jiffies(blink_period));

    return 0;
}

static void __exit rpi_blink_exit(void)
{
    printk(KERN_INFO LKM_NAME ": finishing\n");

    // TODO toggle all pins down
    iounmap(g_gpio_regs);

    del_timer(&blink_timer);
}

module_init(rpi_blink_init);
module_exit(rpi_blink_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION(LKM_NAME " - kernel module to interact with leds");
MODULE_VERSION("1.0");
