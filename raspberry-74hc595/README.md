## Raspberry Pi blink module

A kernel module for Raspberry Pi to periodically turn on and off a single LED
attacked to GPIO pin 18.

This module uses code from `https://github.com/vovkos/rpi-gpio-test` for GPIO
handling and from `https://github.com/sysprogs/tutorials` for the timer.

### Building and launching

Wire up the Raspberry Pi, LEDs and a 7cHC595N according to following schematics:

![schematics](./schematics.png?raw=true "schematics")

To build the module an up to date Raspberry Pi OS is advised. It is also
necessary to install kernel header files:

```bash
sudo apt install raspberrypi-kernel-headers
```

To build the module enter `src` directory and issue `make` command. If the kernel
module was built successfully there should be a file `rpi-74hc595-blink.ko`.
To load the module and start showing the byte pattern enter following command:

```bash
sudo insmod rpi-74hc595-blink.ko
```

To unload the module:

```bash
sudo rmmod rpi-74hc595-blink.ko
```
