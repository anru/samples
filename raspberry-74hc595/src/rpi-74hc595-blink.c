/*
 * Based on
 *  1. https://github.com/vovkos/rpi-gpio-test
 *  2. https://github.com/sysprogs/tutorials
 *
 * Use Raspberry Pi to control a SN74HC595N chip to light eight leds.
 * The Pi will use GPIO pint 17, 18, 23 to control the chip.
 *
 * Every 100 milliseconds (controlled by blink_period) Pi will write a byte
 * to the chip by first enabling write (GPIO pin 17 low), writing each bit to
 * GPIO pin 23, high or low depending whether the bit is 1 or 0 respectively
 * and pulsing the clock pin (GPIO 18) high to low for chip to accept the value.
 *
 * Seems to be unstable; leds are lighting up correctly for several seconds
 * and then gaps start to appear.
 */

#include <linux/init.h>
#include <linux/module.h>
#include <linux/timer.h>
#include <linux/gpio.h>
#include <linux/sched.h>
#include <linux/delay.h>

#define LKM_NAME "rpi-74hc595-blink"

/*
 * Taken from vovkos/rpi-gpio-test "as is".
 */
static volatile unsigned int *g_gpio_regs;

#define GPIO_BASE_ADDR 0x3f200000

#define GPIO_SET_FUNC_IN(g) *(g_gpio_regs + ((g) / 10)) &= ~(7 << (((g) % 10) * 3))
#define GPIO_SET_FUNC_OUT(g) *(g_gpio_regs + ((g) / 10)) |= (1 << (((g) % 10) * 3))

#define GPIO_GET(g) ((*(g_gpio_regs + 13) & (1 << (g))) != 0)
#define GPIO_SET(g) (*(g_gpio_regs + 7) = 1 << (g))
#define GPIO_CLR(g) (*(g_gpio_regs + 10) = 1 << (g))

#define GPIO_LATCH_OUT 17
#define GPIO_CLOCK_OUT 18
#define GPIO_DATA_OUT 23

#define HIGH true
#define LOW false

static struct timer_list blink_timer;
/* LEDs will be updated every 500ms */
static int blink_period = 500;

uint8_t rotate_right(uint8_t value)
{
    /*
     * shift byte right and preserve rightmost bit
     * by prepending it
     */
    return (value >> 1) | (value << 7);
}

static void set_GPIO(int pin, bool value)
{
    if (value) {
        GPIO_SET(pin);
    } else {
        GPIO_CLR(pin);
    }
}

static void tick(int clock_pin)
{
    /*
     * TODO use delays for stability?
     *
     * But this does not really affect the stability displayed value
     *
     * static int clock_period = 30; // in microseconds
     * set_GPIO(clock_pin, HIGH);
     * udelay(clock_period);
     * set_GPIO(clock_pin, LOW);
     * udelay(clock_period);
     */

    set_GPIO(clock_pin, HIGH);
    set_GPIO(clock_pin, LOW);
}

static void shift_out(int clock_pin, int data_pin, uint8_t value)
{
    int mask = 0x80;
    while (mask > 0) {
        set_GPIO(data_pin, (value & mask) ? HIGH : LOW);
        mask = mask >> 1;

        tick(clock_pin);
    }
}

static void timer_handler(struct timer_list *unused)
{
    static uint8_t value = 0x0f;

    /*
     * In arduino example the setup is as follows:
     *
     * GPIO_CLR(GPIO_LATCH_OUT);
     * shift_out(GPIO_CLOCK_OUT, GPIO_DATA_OUT, value);
     * GPIO_SET(GPIO_LATCH_OUT);
     */

    GPIO_CLR(GPIO_LATCH_OUT);
    tick(GPIO_CLOCK_OUT);

    shift_out(GPIO_CLOCK_OUT, GPIO_DATA_OUT, value);

    GPIO_SET(GPIO_LATCH_OUT);
    tick(GPIO_CLOCK_OUT);
    GPIO_CLR(GPIO_LATCH_OUT);
    tick(GPIO_CLOCK_OUT);

    value = rotate_right(value);

    mod_timer(&blink_timer, jiffies + msecs_to_jiffies(blink_period));
}

static int __init rpi_74hc595_blink_init(void)
{
    int result;

    printk(KERN_INFO LKM_NAME ": initializing\n");

    g_gpio_regs = (volatile unsigned int *)ioremap(GPIO_BASE_ADDR, 16 * 1024);
    if (!g_gpio_regs) {
        printk(KERN_INFO LKM_NAME ": could not map GPIO registers\n");
        return -EBUSY;
    }

    GPIO_SET_FUNC_OUT(GPIO_LATCH_OUT);
    GPIO_SET_FUNC_OUT(GPIO_CLOCK_OUT);
    GPIO_SET_FUNC_OUT(GPIO_DATA_OUT);

    timer_setup(&blink_timer, timer_handler, 0);
    result = mod_timer(&blink_timer, jiffies + msecs_to_jiffies(blink_period));

    return 0;
}

static void __exit rpi_74hc595_blink_exit(void)
{
    printk(KERN_INFO LKM_NAME ": finishing\n");

    GPIO_CLR(GPIO_LATCH_OUT);
    GPIO_CLR(GPIO_CLOCK_OUT);
    GPIO_CLR(GPIO_DATA_OUT);

    iounmap(g_gpio_regs);

    del_timer(&blink_timer);
}

module_init(rpi_74hc595_blink_init);
module_exit(rpi_74hc595_blink_exit);

MODULE_LICENSE("GPL");
MODULE_DESCRIPTION(LKM_NAME " - kernel module to interact with leds");
MODULE_VERSION("1.0");
