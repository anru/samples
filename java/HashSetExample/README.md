## Hash set example

An application to create and populate an array of integers in Java.
Files available at `https://bitbucket.org/anru/samples/`.

### Building and launching

To build the application use Gradle:

```bash
./gradlew build
```

This should produce a `build` directory with intermediate build files and the
main class file. To launch the application use `java` command:

```bash
java -Xmx32g -classpath build/classes/java/main HashSetExample.Main
```

Storing large number of integers is heavy on memory so the switch `-Xmxi32g`
is important.

### Options and commands

Application recognizes two command line parameters: `-o` and `-r`.
Parameter `-o` allows to specify a file where to store of from which to load
a serialized representation of the hash set.
Parameter `-r` allows to populate the hash set with a number of random values.

When launched application prompts user to input a command. Recognized commands
are `?`, `p`, `c`, `a`, `d`. Commands `a` and `d` take an integer as additional
parameter.

```text
    a <number> add a number
    d <number> remove a number
    p          show set contents
    ?          show this help
    c          exit
```
