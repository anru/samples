/*
 * Hash set example.
 *
 * An application to create and populate an array of integers backed by
 * a hash set in Java.
 */
package HashSetExample;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * ./gradlew build
 * java -Xmx32g -classpath build/classes/java/main HashSetExample.Main
 *
 * If n is constant (O(n)) array is quite suited for storing integers.
 */
public class Main {

    private static final int MAX_COUNT = 100000000;

    private static String fileName = null;
    private static int count = 0;

    private static String commandRegex = "([ad]) ([0-9]{1,10})";
    private static Pattern commandPattern = Pattern.compile(commandRegex);

    /**
     */
    public static void main(String[] args) {
        parseArgs(args);

        HashSet<Integer> hashSet;
        if (fileName != null && new File(fileName).exists()) {
            System.out.printf("Serialized hash map %s found\n", fileName);
            hashSet = deserializeSet(fileName);

            if (hashSet == null) {
                System.out.println("Failed to deserialize hash set from " + fileName);
                System.exit(1);
            }

            System.out.printf("Serialized hash set loaded with %d items\n", hashSet.size());
        } else {
            System.out.println("Creating a new hash set");

            hashSet = new HashSet<>();
            populateSet(hashSet, count);
        }

        inputLoop(hashSet);

        if (fileName != null) {
            serializeSet(fileName, hashSet);
        }
    }

    private static void parseArgs(String[] args) {
        List<String> list = Arrays.asList(args);
        Iterator<String> iterator = list.iterator();
        while (iterator.hasNext()) {
            String arg = iterator.next();
            switch (arg) {
                case "-o":
                    if (iterator.hasNext()) {
                        fileName = iterator.next();
                    } else {
                        System.err.println("Option `-o` requires a string parameter.\n");
                        System.exit(1);
                    }
                    break;
                case "-r":
                    if (iterator.hasNext()) {
                        try {
                            count = parseInteger(iterator.next());
                        } catch (Exception e) {
                            System.exit(1);
                        }
                    } else {
                        System.err.println("Option `-r` requires a positive integer parameter.\n");
                        System.exit(1);
                    }
                    if (count > MAX_COUNT) {
                        count = MAX_COUNT;
                    }
                    if (count < 0) {
                        count = 0;
                    }
                    break;
            }
        }
    }

    private static void inputLoop(HashSet<Integer> set) {
        Scanner scanner = new Scanner(System.in);
        while (true) {
            System.out.print("Enter a number (c to exit): ");
            String input = scanner.nextLine();
            if ("c".equals(input) || "q".equals(input)) {
                break;
            }
            if ("p".equals(input)) {
                dump(set);
                continue;
            }
            if ("?".equals(input)) {
                usage();
                continue;
            }

            Matcher matcher = commandPattern.matcher(input);
            if (!matcher.matches()) {
                System.out.println(input + " did not match command");
                continue;
            }

            String command = matcher.group(1);
            String value = matcher.group(2);

            try {
                int number = parseInteger(value);
                if (set.contains(number)) {
                    if ("d".equals(command)) {
                        System.out.printf("Value %d removed from set\n", number);
                        set.remove(number);
                    } else {
                        System.out.printf("Value %d already in hash set\n", number);
                    }
                } else {
                    if ("a".equals(command)) {
                        if (set.add(number)) {
                            System.out.printf("Value %d added to set\n", number);
                        }
                    } else {
                        System.out.printf("Value %d not found in hash set\n", number);
                    }
                }
            } catch (Exception e) {
                // do nothing, continue with loop
            }
        }
    }

    private static void dump(HashSet<Integer> set) {
        int i = 0;
        for (Integer value : set) {
            System.out.printf("    %d: %d\n", i, value);
            i++;
        }
    }

    private static void usage() {
        System.out.println("    a <number> add a number");
        System.out.println("    d <number> remove a number");
        System.out.println("    p          show set contents");
        System.out.println("    ?          show this help");
        System.out.println("    c          exit");
    }

    private static void populateSet(HashSet<Integer> set, int count) {
        System.out.println(">    Populating hash set");
        long start = System.currentTimeMillis();

        int i = 0;
        int step = 1000000;
        while (set.size() < count) {
            Integer value = randomInteger();
            set.add(value);

            if (i > 0 && (i % step) == 0) {
                int size = set.size();
                System.out.printf("      Hash set populated with %d items (at %d iterations)\n",
                        size, i);
                if (size > step * 100) {
                    step *= 10;
                }
            }
            i++;
        }
        System.out.printf("<    Populating hash set finished in %dms\n",
                System.currentTimeMillis() - start);
    }

    private static void serializeSet(String fileName, HashSet<Integer> set) {
        System.out.println(">    Serializing hash set");
        long start = System.currentTimeMillis();

        try {
            FileOutputStream fos = new FileOutputStream(fileName);
            ObjectOutputStream oos = new ObjectOutputStream(fos);

            oos.writeObject(set);

            oos.close();
            fos.close();
        } catch(IOException e) {
            e.printStackTrace();
        }
        System.out.printf("<    Serializing hash set finished in %dms\n",
                System.currentTimeMillis() - start);
    }

    private static HashSet<Integer> deserializeSet(String fileName) {
        long start = System.currentTimeMillis();
        System.out.println(">    Deserializing hash set");
        HashSet<Integer> map = null;
        try {
            FileInputStream fis = new FileInputStream(fileName);
            ObjectInputStream ois = new ObjectInputStream(fis);

            map = (HashSet<Integer>) ois.readObject();

            ois.close();
            fis.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        System.out.printf("<    Deserializing hash set finished in %dms\n",
                System.currentTimeMillis() - start);
        return map;
    }

    private static int randomInteger() {
        Random random = new Random();
        return random.nextInt(500000001);
    }

    private static int parseInteger(String s) {
        int result = 0;
        try {
             result = Integer.parseInt(s);
        } catch (NumberFormatException e) {
            System.err.printf("%s is not an integer.\n", s);
            throw e;
        }
        return result;
    }
}
